package com.myzee.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_EMP")
public class EmployeeEntity {
	
	@Id
	@GeneratedValue
	private long id;
	
	@Column(name = "f_name")
	private String fName;
	
	@Column(name = "l_name")
	private String lName;
	
	@Column(name = "email")
	private String email;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "id: " + id
				+ " fname: " + fName
				+ " lname: " + lName
				+ " email: " + email;
	}
	
}
