package com.myzee.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myzee.springboot.exception.RecordNotFoundException;
import com.myzee.springboot.model.EmployeeEntity;
import com.myzee.springboot.service.EmployeeService;

@RestController
@RequestMapping("/e")
public class EContoller {
	
	@Autowired
	EmployeeService eserv;
	
	@GetMapping (produces = "application/json")
	public ResponseEntity<List<EmployeeEntity>> getAllEs() {
		List<EmployeeEntity> list = eserv.getAllEmployees();
		return new ResponseEntity<List<EmployeeEntity>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<EmployeeEntity> getById(@PathVariable("id") Long id) throws RecordNotFoundException {
		EmployeeEntity eet = eserv.getEmpById(id);
		return new ResponseEntity<EmployeeEntity>(eet, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<EmployeeEntity> createOrUpdateE(EmployeeEntity e) {
		EmployeeEntity updated = eserv.createOrUpdateEmployee(e);
		return new ResponseEntity<EmployeeEntity>(updated, new HttpHeaders(), HttpStatus.OK);
	}
	
	@DeleteMapping("/{eid}")
	public HttpStatus deleteEById(@PathVariable("eid") Long eid) {
		eserv.deleteById(eid);
		return HttpStatus.FORBIDDEN;
	}
}
