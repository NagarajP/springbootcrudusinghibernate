package com.myzee.springboot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myzee.springboot.dao.EmployeeRepository;
import com.myzee.springboot.exception.RecordNotFoundException;
import com.myzee.springboot.model.EmployeeEntity;

@Service
public class EmployeeService {
	@Autowired
	EmployeeRepository repository;
	
	public List<EmployeeEntity> getAllEmployees() {
		List<EmployeeEntity> empList = repository.findAll();
		if(empList.isEmpty()) {
			return new ArrayList<EmployeeEntity>();
		} else {
			return empList;
		}
	}
	
	public EmployeeEntity getEmpById(Long eid) throws RecordNotFoundException {
		Optional<EmployeeEntity> e = repository.findById(eid);
		
		if(e.isPresent()) {
			return e.get();
		} else {
			throw new RecordNotFoundException("No employee record exist for given id");
		}
	}
	
	public EmployeeEntity createOrUpdateEmployee(EmployeeEntity e) {
		Optional<EmployeeEntity> em = repository.findById(e.getId());
		if(em.isPresent()) {
			EmployeeEntity newEntity = em.get();
			newEntity.setId(e.getId());
			newEntity.setfName(e.getfName());
			newEntity.setlName(e.getlName());
			newEntity.setEmail(e.getEmail());
			newEntity = repository.save(newEntity);
			return newEntity;
		} else {
			e = repository.save(e);
			return e;
		}
	}
	
	public void deleteById(Long eid) {
		Optional<EmployeeEntity> ent = repository.findById(eid);
		if(ent.isPresent()) {
			repository.deleteById(eid);
		} else {
			try {
				throw new RecordNotFoundException("record not found, delete unsuccessful");
			} catch (RecordNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
